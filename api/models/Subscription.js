/**
 * Subscription
 *
 * @module      :: Model
 * @description :: A short summary of how this model works and what it represents.
 *
 */

module.exports = {
  schema : true,
  attributes: {

    /* e.g.
  	nickname: 'string'
  	*/
    id: 'int',
    gmail: 'string',
    calendarId: 'string',
    grant: {
      type: 'boolean'
    }
  }

};